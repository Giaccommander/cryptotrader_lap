USE CryptoTrader;
GO

CREATE UNIQUE INDEX uiUser_email ON [User] (email);
GO

CREATE UNIQUE INDEX ui_BankAccount_user ON BankAccount(User_id);
GO

CREATE UNIQUE INDEX ui_BankAccount_iban_bic ON BankAccount (iban,bic);
GO

CREATE UNIQUE INDEX ui_Address_user ON [Address] (User_id);
GO

CREATE UNIQUE INDEX ui_Upload_user ON Upload (User_id);
GO

CREATE UNIQUE INDEX uiCountry_name ON Country ([name]);
GO

CREATE UNIQUE INDEX uiCountry_iso ON Country (iso);
GO