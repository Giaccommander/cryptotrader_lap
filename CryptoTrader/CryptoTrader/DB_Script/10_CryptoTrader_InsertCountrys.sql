INSERT INTO Country (iso,[name]) VALUES

('AFG', 'Afghanistan'),

('ALB', 'Albanien'),

('ATA', 'Antarktis'),

('DZA', 'Algerien'),

('ASM', 'Amerikanisch-Samoa'),

('AND', 'Andorra'),

('AGO', 'Angola'),

('ATG', 'Antigua und Barbuda'),

('AZE', 'Aserbaidschan'),

('ARG', 'Argentinien'),

('AUS', 'Australien'),

('AUT', 'Österreich'),

('BHS', 'Bahamas'),

('BHR', 'Bahrain'),

('BGD', 'Bangladesch'),

('ARM', 'Armenien'),

('BRB', 'Barbados'),

('BEL', 'Belgien'),

('BMU', 'Bermuda'),

('BTN', 'Bhutan'),

('BOL', 'Bolivien'),

('BIH', 'Bosnien und Herzegowina'),

('BWA', 'Botswana'),

('BVT', 'Bouvetinsel'),

('BRA', 'Brasilien'),

('BLZ', 'Belize'),

('IOT', 'Britisches Territorium im Indischen Ozean'),

('SLB', 'Salomonen'),

('VGB', 'Britische Jungferninseln'),

('BRN', 'Brunei Darussalam'),

('BGR', 'Bulgarien'),

('MMR', 'Myanmar'),

('BDI', 'Burundi'),

('BLR', 'Belarus'),

('KHM', 'Kambodscha'),

('CMR', 'Kamerun'),

('CAN', 'Kanada'),

('CPV', 'Kap Verde'),

('CYM', 'Kaimaninseln'),

('CAF', 'Zentralafrikanische Republik'),

('LKA', 'Sri Lanka'),

('TCD', 'Tschad'),

('CHL', 'Chile'),

('CHN', 'China'),

('TWN', 'Taiwan'),

('CXR', 'Weihnachtsinsel'),

('CCK', 'Kokosinseln'),

('COL', 'Kolumbien'),

('COM', 'Komoren'),

('MYT', 'Mayotte'),

('COG', 'Republik Kongo'),

('COD', 'Demokratische Republik Kongo'),

('COK', 'Cookinseln'),

('CRI', 'Costa Rica'),

('HRV', 'Kroatien'),

('CUB', 'Kuba'),

('CYP', 'Zypern'),

('CZE', 'Tschechische Republik'),

('BEN', 'Benin'),

('DNK', 'Dänemark'),

('DMA', 'Dominica'),

('DOM', 'Dominikanische Republik'),

('ECU', 'Ecuador'),

('SLV', 'El Salvador'),

('GNQ', 'Äquatorialguinea'),

('ETH', 'Äthiopien'),

('ERI', 'Eritrea'),

('EST', 'Estland'),

('FRO', 'Färöer'),

('FLK', 'Falklandinseln'),

('SGS', 'Südgeorgien und die Südlichen Sandwichinseln'),

('FJI', 'Fidschi'),

('FIN', 'Finnland'),

('ALA', 'Äland-Inseln'),

('FRA', 'Frankreich'),

('GUF', 'Französisch-Guayana'),

('PYF', 'Französisch-Polynesien'),

('ATF', 'Französische Süd- und Antarktisgebiete'),

('DJI', 'Dschibuti'),

('GAB', 'Gabun'),

('GEO', 'Georgien'),

('GMB', 'Gambia'),

('PSE', 'Palästinensische Autonomiegebiete'),

('DEU', 'Deutschland'),

('GHA', 'Ghana'),

('GIB', 'Gibraltar'),

('KIR', 'Kiribati'),

('GRC', 'Griechenland'),

('GRL', 'Grönland'),

('GRD', 'Grenada'),

('GLP', 'Guadeloupe'),

('GUM', 'Guam'),

('GTM', 'Guatemala'),

('GIN', 'Guinea'),

('GUY', 'Guyana'),

('HTI', 'Haiti'),

('HMD', 'Heard und McDonaldinseln'),

('VAT', 'Vatikanstadt'),

('HND', 'Honduras'),

('HKG', 'Hongkong'),

('HUN', 'Ungarn'),

('ISL', 'Island'),

('IND', 'Indien'),

('IDN', 'Indonesien'),

('IRN', 'Islamische Republik Iran'),

('IRQ', 'Irak'),

('IRL', 'Irland'),

('ISR', 'Israel'),

('ITA', 'Italien'),

('CIV', 'slonoviny'),

('JAM', 'Jamaika'),

('JPN', 'Japan'),

('KAZ', 'Kasachstan'),

('JOR', 'Jordanien'),

('KEN', 'Kenia'),

('PRK', 'Demokratische Volksrepublik Korea'),

('KOR', 'Republik Korea'),

('KWT', 'Kuwait'),

('KGZ', 'Kirgisistan'),

('LAO', 'Demokratische Volksrepublik Laos'),

('LBN', 'Libanon'),

('LSO', 'Lesotho'),

('LVA', 'Lettland'),

('LBR', 'Liberia'),

('LBY', 'Libysch-Arabische Dschamahirija'),

('LIE', 'Liechtenstein'),

('LTU', 'Litauen'),

('LUX', 'Luxemburg'),

('MAC', 'Macao'),

('MDG', 'Madagaskar'),

('MWI', 'Malawi'),

('MYS', 'Malaysia'),

('MDV', 'Malediven'),

('MLI', 'Mali'),

('MLT', 'Malta'),

('MTQ', 'Martinique'),

('MRT', 'Mauretanien'),

('MUS', 'Mauritius'),

('MEX', 'Mexiko'),

('MCO', 'Monaco'),

('MNG', 'Mongolei'),

('MDA', 'Moldawien'),

('MSR', 'Montserrat'),

('MAR', 'Marokko'),

('MOZ', 'Mosambik'),

('OMN', 'Oman'),

('NAM', 'Namibia'),

('NRU', 'Nauru'),

('NPL', 'Nepal'),

('NLD', 'Niederlande'),

('ANT', 'Niederländische Antillen'),

('ABW', 'Aruba'),

('NCL', 'Neukaledonien'),

('VUT', 'Vanuatu'),

('NZL', 'Neuseeland'),

('NIC', 'Nicaragua'),

('NER', 'Niger'),

('NGA', 'Nigeria'),

('NIU', 'Niue'),

('NFK', 'Norfolkinsel'),

('NOR', 'Norwegen'),

('MNP', 'Nördliche Marianen'),

('UMI', 'Amerikanisch-Ozeanien'),

('FSM', 'Mikronesien'),

('MHL', 'Marshallinseln'),

('PLW', 'Palau'),

('PAK', 'Pakistan'),

('PAN', 'Panama'),

('PNG', 'Papua-Neuguinea'),

('PRY', 'Paraguay'),

('PER', 'Peru'),

('PHL', 'Philippinen'),

('PCN', 'Pitcairninseln'),

('POL', 'Polen'),

('PRT', 'Portugal'),

('GNB', 'Guinea-Bissau'),

('TLS', 'Timor-Leste'),

('PRI', 'Puerto Rico'),

('QAT', 'Katar'),

('REU', 'Räunion'),

('ROU', 'Rumänien'),

('RUS', 'Russische Föderation'),

('RWA', 'Ruanda'),

('SHN', 'St. Helena'),

('KNA', 'St. Kitts und Nevis'),

('AIA', 'Anguilla'),

('LCA', 'St. Lucia'),

('SPM', 'St. Pierre und Miquelon'),

('VCT', 'St. Vincent und die Grenadinen'),

('SMR', 'San Marino'),

('STP', 'Sao Tome and Principe'),

('SAU', 'Saudi-Arabien'),

('SEN', 'Senegal'),

('SYC', 'Seychellen'),

('SLE', 'Sierra Leone'),

('SGP', 'Singapur'),

('SVK', 'Slowakei'),

('VNM', 'Vietnam'),

('SVN', 'Slowenien'),

('SOM', 'Somalia'),

('ZAF', 'Südafrika'),

('ZWE', 'Simbabwe'),

('ESP', 'Spanien'),

('ESH', 'Westsahara'),

('SDN', 'Sudan'),

('SSD', 'Südsudan'),

('SUR', 'Suriname'),

('SJM', 'Svalbard and Jan Mayen'),

('SWZ', 'Swasiland'),

('SWE', 'Schweden'),

('CHE', 'Schweiz'),

('SYR', 'Arabische Republik Syrien'),

('TJK', 'Tadschikistan'),

('THA', 'Thailand'),

('TGO', 'Togo'),

('TKL', 'Tokelau'),

('TON', 'Tonga'),

('TTO', 'Trinidad und Tobago'),

('ARE', 'Vereinigte Arabische Emirate'),

('TUN', 'Tunesien'),

('TUR', 'Türkei'),

('TKM', 'Turkmenistan'),

('TCA', 'Turks- und Caicosinseln'),

('TUV', 'Tuvalu'),

('UGA', 'Uganda'),

('UKR', 'Ukraine'),

('MKD', 'Ehem. jugoslawische Republik Mazedonien'),

('EGY', 'Ägypten'),

('GBR', 'Vereinigtes Königreich von Großbritannien'),

('IMN', 'Insel Man'),

('TZA', 'Vereinigte Republik Tansania'),

('USA', 'Vereinigte Staaten von Amerika'),

('VIR', 'Amerikanische Jungferninseln'),

('BFA', 'Burkina Faso'),

('URY', 'Uruguay'),

('UZB', 'Uzbekistan'),

('VEN', 'Venezuela'),

('WLF', 'Wallis und Futuna'),

('WSM', 'Samoa'),

('YEM', 'Jemen'),

('SCG', 'Serbien und Montenegro'),

('ZMB', 'Sambia');
GO
