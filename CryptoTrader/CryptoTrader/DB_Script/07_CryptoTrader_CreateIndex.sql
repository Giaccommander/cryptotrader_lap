USE CryptoTrader;
GO

CREATE INDEX ix_User_created ON [User](created);
GO

CREATE INDEX ix_Balance_created ON Balance(User_id);
GO

CREATE INDEX ix_Upload_created ON Upload(created);
GO

CREATE INDEX ix_Ticker_created ON Ticker(created);
GO

CREATE INDEX ix_BankTransferHistory ON BankTransferHistory(User_id);
GO

CREATE INDEX ix_TradeHistory_created_user ON TradeHistory(User_id,created);
GO

CREATE INDEX ix_Address_created ON [Address](created);
GO

CREATE INDEX ix_City_created ON City(created);
GO

CREATE INDEX ix_Country_created ON Country(created);
GO

USE master;
GO