	ALTER TABLE [Address]
	ADD
		CONSTRAINT fk_Address_City
			FOREIGN KEY (City_id)
				REFERENCES City(id);
	GO

	ALTER TABLE City
	ADD
		CONSTRAINT fk_City_Country
			FOREIGN KEY (Country_id)
				REFERENCES Country(iso);
	GO

	ALTER TABLE BankAccount
	ADD
		CONSTRAINT fk_BankAccount_User
			FOREIGN KEY (User_id)
				REFERENCES [User](id);
	GO

	ALTER TABLE Upload
	ADD
		CONSTRAINT fk_Upload_User
			FOREIGN KEY (User_id)
				REFERENCES [User](id);
	GO

	ALTER TABLE Balance
		ADD
		CONSTRAINT fk_Balance_User
			FOREIGN KEY (User_id)
				REFERENCES [User](id);
	GO

	ALTER TABLE BankTransferHistory
		ADD
		CONSTRAINT fk_BankTransferHistory_User
			FOREIGN KEY (User_id)
				REFERENCES [User](id);
	GO

	ALTER TABLE TradeHistory
		ADD
		CONSTRAINT fk_TradeHistory_User
			FOREIGN KEY (User_id)
				REFERENCES [User](id);
	GO
	ALTER TABLE TradeHistory
		ADD
		CONSTRAINT fk_TradeHistory_Ticker
			FOREIGN KEY (Ticker_id)
				REFERENCES Ticker(id);
	GO
